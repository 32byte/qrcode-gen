# QRCode Generator

## About
Generates a qrcode composed of a base-url (hardcoded in [here](./src/index.js)) and a custom input from the input field.

## How to setup
Simply go [here](https://32byte.gitlab.io/qrcode-gen/) to check out the project.

Alternatively you can run a http server in the [src directory](./src) or open [src/index.html](./src/index.html) in your browser.