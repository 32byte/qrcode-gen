const base_url = "http://www.test.ch/index.html?ref=";

const qrcode_div = document.getElementById("qrcode");
const input_field = document.getElementById("text-input");

var qrcode = new QRCode(qrcode_div, base_url);

function generateQRCode() {
    const input = input_field.value;

    qrcode.makeCode(base_url + input);
}